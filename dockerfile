# Production Build
# Stage 1: Build react client
FROM node:16-alpine as client
# Working directory be app
WORKDIR /usr/app/frontend/
COPY frontend/package*.json ./
# install dependencies
RUN npm install
# copy local files to app folder
COPY frontend/ ./
RUN npm run build

# Stage 2 : Build Server
#FROM ubuntu:jammy
FROM node:16-alpine
ARG DEBIAN_FRONTEND=noninteractive
#RUN apt-get update && apt-get install -y nodejs npm libeigen3-dev libboost-all-dev libopencv-dev libpcl-dev zlib1g-dev libflann-dev libqhull-dev libsnappy-dev libicu-dev libglib2.0-dev libbsd-dev libpcre3-dev libbson-dev libmongoc-dev libcgal-dev
WORKDIR /usr/src/app/
COPY --from=client /usr/app/frontend/build/ ./frontend/build/
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 4000
CMD ["npm", "start"]
